# doctor

> A Mpvue project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
``` bash
# 页面开发分配
1.首页 - 腿子
2.问诊 - 晓玲
3.问诊（未评价）- 晓玲
4.问诊（已评价）- 晓玲
5.义诊列表 - 小罗
6.义诊专家介绍 - 小罗
7.医生列表 - 小罗
8.图文咨询-专家介绍 - 小罗
9.图文咨询-订单确认 - 邹邹
10.图文咨询-已选就诊人 - 邹邹
11.图文咨询-已选就诊人 - 邹邹
12.图文咨询-添加就诊人 - 猴哥
13.图文咨询-问诊间咨询 - 晓玲
14.图文咨询-处方笺 - 腿子
15.图文咨询-处方笺（已缴费） - 晓玲
16.图文咨询-明细 - 邹邹
17.电话咨询-专家介绍 - 小罗
18.电话咨询-就诊资料 - 邹邹
19.电话咨询-购买成功 - 小罗
20.电话咨询-处方笺 - 腿子
21.服务评价 - 猴哥
22.视频咨询-专家介绍 - 腿子
23.视频咨询-就诊资料 - 猴哥
24.既往史 - 猴哥
25.既往史 - 猴哥
26.过敏史 - 小罗
27.家庭史 - 小罗
28.个人习惯 - 小罗
29.视频咨询时间 - 腿子
30.视频问诊开处方 - 腿子
31.视频咨询-开处方 - 腿子
32.个人中心 - 邹邹
33.我的订单 - 腿子
34.订单详情 - 腿子
35.我的处方 - 邹邹
36.处方笺 - 邹邹

#项目结构
 Admin
 |-- build                            // 项目构建(webpack)相关代码
 |   |-- build.js                     // 生产环境构建代码
 |   |-- check-version.js             // 检查node、npm等版本
 |   |-- dev-client.js                // 热重载相关
 |   |-- dev-server.js                // 构建本地服务器
 |   |-- utils.js                     // 构建工具相关
 |   |-- webpack.base.conf.js         // webpack基础配置
 |   |-- webpack.dev.conf.js          // webpack开发环境配置
 |   |-- webpack.prod.conf.js         // webpack生产环境配置
 |-- config                           // 项目开发环境配置
 |   |-- dev.env.js                   // 开发环境变量
 |   |-- index.js                     // 项目一些配置变量
 |   |-- prod.env.js                  // 生产环境变量
 |-- src                              // 源码目录
 |   |-- common                       // 项目公共方法接Api接口
 |   |   |-- api                      // 项目api接口文件存放目录
 |   |   |-- base                     // 项目基础参数配置 与运行环境无关
 |   |   |-- method                   // 项目公共工具类
 |   |-- components                   // vue公共组件
 |   |   |-- Main                     // 页面主视图容器组件
 |   |   |-- Maps                     // 项目公共地图组件
 |   |   |-- ScrollPane               // 标签导航公共组件
 |   |-- router                       // vue项目路由
 |   |-- store                        // vuex的状态管理
 |   |-- views                        // view视图层
 |   |   |-- xxxxx                    // xxx
 |   |-- App.vue                      // 页面入口文件
 |   |-- main.js                      // 程序入口文件，加载各种公共组件
 |-- static                           // 静态文件，比如一些图片，json数据等
 |   |--css                           // 项目公共样式类 less
 |   |--font                          // 项目字体库存放目录
 |   |--img                           // 项目图片文件夹
 |   |--js                            // 项目依赖 如i18n 语言包等
 |-- .babelrc                         // ES6语法编译配置
 |-- .editorconfig                    // 定义代码格式
 |-- .gitignore                       // git上传需要忽略的文件格式
 |-- README.md                        // 项目说明
 |-- favicon.ico
 |-- index.html                       // 入口页面
 |-- package.json                     // 项目基本信息
 ```